package com.miramicodigo.recyclerviewsimple

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



    }

    fun llenarDatos(): ArrayList<String> {
        var datos = ArrayList<String>()
        for (i in 0..500) {
            datos.add("Item en posicion: ${i + 1}")
        }
        return datos
    }

    inner class RecyclerViewHolder(itemView: View) {
        var tvTexto : TextView = itemView as TextView

        init {



        }
    }

    inner class RecyclerViewAdapter(val data: ArrayList<String>) {


    }
}
